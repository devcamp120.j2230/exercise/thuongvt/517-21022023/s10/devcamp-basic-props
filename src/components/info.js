import { Component } from "react";

class Info extends Component {
    render(){
        console.log(this.props)
        // có thể sử dụng phép gán phá hủy cấu trúc đối tượng để giảm thiểu phải viết cấu trúc this.props.
        return(
            <>
                Tên là {this.props.firstName} {this.props.lastName} tuổi là {this.props.favNumber}
                <p>subject: {this.props.children}</p>
            </>
        )
    }
}

export default Info